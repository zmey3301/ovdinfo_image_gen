FROM debian:buster-slim

RUN apt update \
    && apt -y --no-install-recommends install xvfb python3 python3-pip python3-setuptools \
    && apt -y --no-install-recommends install wkhtmltopdf xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic \
    && pip3 install wheel

RUN groupadd flask && useradd -g flask flask

RUN mkdir /app_root
RUN chown flask:flask /app_root
COPY --chown=flask:flask . /app_root/
WORKDIR /app_root
RUN pip3 install -r requirements.txt
USER flask
RUN chmod u+x entrypoint.sh
EXPOSE 5000 5000/tcp
ENV SECRET ""
ENTRYPOINT ["/app_root/entrypoint.sh"]