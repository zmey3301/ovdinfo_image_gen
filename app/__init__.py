from flask import Flask
from flask_bootstrap import Bootstrap

application = Flask(__name__)
application.config.from_object("flask-config")
Bootstrap(application)

from app import template_utils
from app import views
