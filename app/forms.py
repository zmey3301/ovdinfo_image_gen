from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SubmitField
from wtforms.validators import DataRequired


class LawCourtForm(FlaskForm):
    heading = StringField("Заголовок", validators=[DataRequired(message="Введите заголовок!")])
    header_text = TextAreaField("Мероприятие",
                                validators=[DataRequired(message="Введите краткое описание мероприятия!")])
    footer_text = TextAreaField("Подвал", validators=[DataRequired(message="Введите текст подвала!")])
    submit_button = SubmitField("Сгенерировать изображение")
