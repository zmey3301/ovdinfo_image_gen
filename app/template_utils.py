from jinja2 import evalcontextfilter
from flask import Markup, escape
from app import application


@evalcontextfilter
@application.template_filter("newline2br")
def newline2br(eval_ctx, value):
    result = escape(value).replace("\n", Markup("<br/>\n"))
    if eval_ctx.autoescape:
        result = Markup(result)
    return result
