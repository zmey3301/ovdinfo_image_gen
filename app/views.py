from os import path
from io import BytesIO
from flask import (
    render_template,
    request,
    send_file
)
from imgkit import from_string as image_from_html
from app import application
from app.forms import LawCourtForm

CSS_ASSETS_PATH = path.abspath("app/assets/css")


@application.route("/", methods=["GET", "POST"])
@application.route("/index", methods=["GET", "POST"])
def index():
    law_court_form = LawCourtForm()
    if request.method == "POST" and law_court_form.validate_on_submit():
        image_html = render_template("images/law-court.html", form_data=law_court_form)
        image = image_from_html(image_html, False, options=application.config["IMAGE_GEN_CONFIG"],
                                css=path.join(CSS_ASSETS_PATH, "ovdinfo-image.css"))
        image_file = BytesIO(image)
        image_file.name = "law_court_image.jpg"
        return send_file(image_file, mimetype="image/jpeg",
                         as_attachment=True, attachment_filename="law_court_image.jpg")

    return render_template("index.html", law_court_form=law_court_form)
