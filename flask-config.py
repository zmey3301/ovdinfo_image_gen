from os import environ
from shutil import which

WTF_CSRF_ENABLED = True
SECRET_KEY = environ["SECRET"]
IMAGE_GEN_CONFIG = {
    "width": 1280,
}

if which("Xvfb") is not None:
    IMAGE_GEN_CONFIG["xvfb"]: True
